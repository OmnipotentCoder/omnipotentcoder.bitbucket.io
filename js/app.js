var pages = new Vue({
  el: '#pages',
  data: {
    page_1_hidden: false,
    page_2_hidden: true,
  }
})

var page_1 = new Vue({
  el: '#page_1',
  data: {
    keyboard_visible: false,
    enter_text: "Next",
    selected: false,
    selected_username: false,
    selected_password: false,
    username: "",
    password: "",
    hide_input: false,
    popup_message_clean: "Authorizing",
    popup_message: "Authorizing.",
    pips: 1,
    popup_visible: false,
    visible: true,
    hidden: false,
    correct_usr: "user-221",
    correct_pwd: "p455w0rd",
  },
  methods: {
    press: function(key){
      if (this.selected_username) {
        this.username += key;
      } else if (this.selected_password) {
        this.password += key;
      }
    },

    backspace: function(event){
      if (this.selected_username) {
        this.username = this.username.substring(0,this.username.length-1);
      } else if (this.selected_password) {
        this.password = this.password.substring(0,this.password.length-1);
      }
    },

    enter: function(event){
      if (this.selected_username) {
        this.click_password();
      } else if (this.selected_password) {
        this.selected = false;
        this.keyboard_visible = false;
        setTimeout(function() {
          page_1.popup_visible = true;
          page_1.login();
        }, 1000);

      }
    },
    click_username: function (event) {
      // `this` inside methods points to the Vue instance
      this.selected = true;
      this.keyboard_visible = true;
      this.selected_username = true;
      this.selected_password = false;
      this.enter_text = "Next";
    },
    click_password: function (event) {
      // `this` inside methods points to the Vue instance
      this.selected = true;
      this.keyboard_visible = true;
      this.selected_username = false;
      this.selected_password = true;
      this.enter_text = "Login";
    },
    correct_password: function (event) {
      if (this.username == this.correct_usr && this.password == this.correct_pwd) {
        return true;
      } else {
        return false;
      }
    },
    page_2: function (event) {
      this.visible = false;
      this.hidden = true;
      page_2.visible = true;
    },
    login: function(event){
      const self = this
      var i = 0;
      const interval = window.setInterval(function(){

        if (i > 13){
          if (i == 14) {
            if (page_1.correct_password()){
              self.popup_message = self.popup_message + "Succes!"
            } else {
              self.popup_message = self.popup_message + "Failed!"
            }
          }
          if (i > 16){
            clearInterval(interval);
            if (page_1.correct_password()){
              self.visible = false;
              setTimeout(function() {
                  self.hidden = true;
                  page_2.visible = true;
              }, 1000);
            } else {
              self.popup_visible = false;
              self.popup_message = self.popup_message_clean + ".";
            }
          }
        } else {
          self.pips = ((self.pips) % 5) + 1;
          self.popup_message = self.popup_message_clean + ".".repeat(self.pips);
        }
        i++;
      }, 500);
    }
  }
})


var page_2 = new Vue({
    el: '#page_2',
    data: {
      visible: false,
      default_motive: "Selecteer Motief",
      default_weapon: "Selecteer Wapen",
      default_victim: "Selecteer Slachtoffer",
      default_suspect: "Selecteer Verdachte",
      up: false,
      case_1: {
        victims: [["Kroongetuige", "Bodyguard", "Officier van Justitie", "Boekhouder", "Escort Dame"]],
        suspects: [["Clive Dalton", "Johnny Richards", "Tony Manning", "Neville Carter", "Kenny Jenkins"]],
        correct_motive: "Angst",
        correct_victim: "Kroongetuige",
        correct_weapon: "Pindakaas",
        correct_suspect: "Kenny Jenkins"
      },
      case_2: {
        victims: [['Benjamin Thompson', 'Bobby Taylor', 'Bryant Terrance', 'Burt Tremblay', 'Dale O\'Ryan'],
                  ['Dave O\'Sullivan', 'Don O\'Connor', 'Jake Bond', 'Ted Williams'],
                  [ 'Thomas O\'Neill', 'Tony Walsh', 'Truman Wakefield', 'Warren Brown']],
        suspects: [['Adam Lancaster', 'Addison Darlington', 'Agnes Livingstone', 'Albert Dudley', 'Alfred Davenport'],
                    ['Amanda Langdon', 'Daniel Cumberbatch', 'David Chatlam', 'Drake Camden']],
        correct_motive: "Overspel",
        correct_victim: "Tony Walsh",
        correct_weapon: "Colt",
        correct_suspect: "Alfred Davenport"
      },
      case_3: {
        victims: [['Caroline Eyre', 'David Travolta', 'Hannah Petite', 'Jacob Young'],
                  ['Jane Young', 'John Collins', 'Phil Montgomery', 'Rose Caqrt3er']],
        suspects: [['Lord Napoleon', 'Lord McDonnahugh', 'Doctor Phil'],
                  [ 'Lord Paul', 'Lord Tullamore', 'Lord Scott']],
        correct_motive: "Wraak",
        correct_victim: 'Jacob Young',
        correct_weapon: "Kandelaar",
        correct_suspect: 'Lord Tullamore'
      },
      current_case: null,
      prompt_visible: false,
      alert_visible: false,
      alert_title: "",
      alert_message: "",
      case: 1,
    },
    methods: {
      init: function (event) {
        this.case_1.selected_weapon = this.default_weapon;
        this.case_1.selected_motive = this.default_motive;
        this.case_1.selected_victim = this.default_victim;
        this.case_1.selected_suspect = this.default_suspect;
        this.case_2.selected_weapon = this.default_weapon;
        this.case_2.selected_motive = this.default_motive;
        this.case_2.selected_victim = this.default_victim;
        this.case_2.selected_suspect = this.default_suspect;
        this.case_3.selected_weapon = this.default_weapon;
        this.case_3.selected_motive = this.default_motive;
        this.case_3.selected_victim = this.default_victim;
        this.case_3.selected_suspect = this.default_suspect;
        this.current_case = this.case_1;
      },
      click_suspect_o: function (suspect){
        this.current_case.selected_suspect = suspect;
        this.$forceUpdate();
      },
      click_victim_o: function (victim){
        this.current_case.selected_victim = victim;
        this.$forceUpdate();
      },
      click_weapon: function(){
        page_murderweapons.hidden = false;
        this.up = true;
        setTimeout(function() {
          page_murderweapons.visible = true;
        }, 10);
      },
      click_motive: function(){
        page_motives.hidden = false;
        this.up = true;
        setTimeout(function() {
          page_motives.visible = true;
        }, 10);
      },
      click_suspect: function(){
        page_suspects.hidden = false;
        this.up = true;
        setTimeout(function() {
          page_suspects.visible = true;
        }, 10);
      },
      click_victim: function(){
        page_victims.hidden = false;
        this.up = true;
        setTimeout(function() {
          page_victims.visible = true;
        }, 10);
      },
      confirm: function(){
        if (this.check_if_selected()) {
        this.prompt_visible = true;
        }
      },
      confirm_no: function(){
        this.prompt_visible = false;
      },
      confirm_yes: function() {
        if (this.check_if_correct()){
          this.alert_title = "Goed gedaan!";
          this.alert_message = "Dit was inderdaad het goede antwoord!\nGa door naar de volgende case!";
          this.alert_visible = true;
          this.prompt_visible = false;
          const self = this;
          setTimeout(function() {
              self.alert_visible = false;
              self.next_case();
          }, 5000);
        } else {
          this.alert_title = "Helaas...";
          this.alert_message = "Dit was helaas niet het goede antwoord...";
          this.alert_visible = true;
          this.prompt_visible = false;
          const self = this;
          setTimeout(function() {
              self.alert_visible = false;
          }, 5000);
        }
      },
      next_case: function(){
        if (this.case == 1) {
          this.current_case = this.case_2;
          this.case = 2
        } else if (this.case == 2){
          this.current_case = this.case_3;
          this.case = 3
        } else {
          this.alert_title = "Eindspel";
          this.alert_message = "Placeholder voor het eindspel";
          this.alert_visible = true;
        }
      },
      check_if_correct: function(){
        return this.current_case.selected_motive == this.current_case.correct_motive &&
                this.current_case.selected_victim == this.current_case.correct_victim &&
                this.current_case.selected_suspect == this.current_case.correct_suspect &&
                this.current_case.selected_weapon == this.current_case.correct_weapon;
      },
      check_if_selected: function() {
        return this.current_case.selected_motive != this.default_motive &&
                this.current_case.selected_victim != this.default_victim &&
                this.current_case.selected_weapon != this.default_weapon &&
                this.current_case.selected_suspect != this.default_suspect;
      }
    },
    beforeMount(){
      this.init();
    },
})

var page_murderweapons = new Vue({
    el: '#page_murderweapons',
    data: {
      visible: false,
      hidden: true,
      weapons: [
        [
          'Boekensteun',
          'Colt',
          'Degen',
          'Flintlock',
          'Golfclub',
        ],[
          'Jachtgeweer',
          'Kandelaar',
          'Kussen',
          'Mauser',
          'Mes',
        ],[
          'Musket',
          'Overdosis',
          'Pindakaas',
          'Pook',
          'Schaaldieren',
        ],[
          'Schep',
          'Shotgun',
          'Strijkijzer',
          'Vuistslag',
          'Zwaard',
        ]
      ],
    },
    methods: {
      click_weapon: function(weapon){
        page_2.current_case.selected_weapon = weapon;
        this.$forceUpdate();
        this.visible = false;
        page_2.up = false;
        const self = this;
        setTimeout(function() {
            self.hidden = true;
        }, 1000);
      }
    }
})

var page_motives = new Vue({
    el: '#page_motives',
    data: {
      visible: false,
      hidden: true,
      motives: [
        [
        'Afgunst',
        'Afkeer',
        'Afwijzing',
        'Angst',
        'Conflict',
      ],[
        'Haat',
        'Hebzucht',
        'Jaloezie',
        'Noodweer',
        'Overspel',
      ],[
        'Per Ongeluk',
        'Politiek',
        'Religie',
        'Vete',
        'Wraak',
      ]
      ]
    },
    methods: {
      click_motive: function(motive){
        page_2.current_case.selected_motive = motive;
        this.$forceUpdate();
        this.visible = false;
        page_2.up = false;
        const self = this;
        setTimeout(function() {
            self.hidden = true;
        }, 1000);
      }
    }
})

var page_suspects = new Vue({
    el: '#page_suspects',
    data: {
      visible: false,
      hidden: true,
    },
    methods: {
      click_suspect: function(suspect){
        page_2.current_case.selected_suspect = suspect;
        this.$forceUpdate();
        this.visible = false;
        page_2.up = false;
        const self = this;
        setTimeout(function() {
            self.hidden = true;
        }, 1000);
      }
    }
})

var page_victims = new Vue({
    el: '#page_victims',
    data: {
      visible: false,
      hidden: true,
    },
    methods: {
      click_victim: function(victim){
        page_2.current_case.selected_victim = victim;
        this.$forceUpdate();
        this.visible = false;
        page_2.up = false;
        const self = this;
        setTimeout(function() {
            self.hidden = true;
        }, 1000);
      }
    }
})
